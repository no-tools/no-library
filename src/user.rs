use std::path::PathBuf;

use sysinfo::SystemExt;
use sysinfo::ProcessExt;
use sysinfo::UserExt;

use crate::Error;

/// Struct representing a user
pub struct User {
    pub uid: u32,
    pub username: String,
    pub dirs: Dirs,
}

/// Struct representing a user's home, config, data and cache directories
pub struct Dirs {
    pub home: PathBuf,
    pub config: PathBuf,
    pub data: PathBuf,
    pub cache: PathBuf,
}

impl User {
    /// Retrieves information about the user running the program
    pub fn new() -> Result<Self, Error> {
        let system = sysinfo::System::new_all();
        let pid = sysinfo::get_current_pid().or(Err(Error::InvalidPid))?;
        let process = system.process(pid).ok_or(Error::InvalidPid)?;

        let uid = process.user_id().ok_or(Error::InvalidUid)?;
        let user = system.get_user_by_id(uid).ok_or(Error::InvalidUid)?;

        let dirs = Dirs::new()?;

        Ok(Self {
            uid: **uid,
            username: user.name().to_string(),
            dirs,
        })
    }
}

impl Dirs {
    /// Retrieves the user's home, config, data and cache directories
    pub fn new() -> Result<Self, Error> {
        let home = dirs::home_dir().ok_or_else(|| Error::InvalidDirs("Home".to_string()))?;
        let config = dirs::config_dir().ok_or_else(|| Error::InvalidDirs("Config".to_string()))?;
        let data = dirs::data_dir().ok_or_else(|| Error::InvalidDirs("Data".to_string()))?;
        let cache = dirs::cache_dir().ok_or_else(|| Error::InvalidDirs("Cache".to_string()))?;

        Ok(Self {
            home,
            config,
            data,
            cache,
        })
    }
}
