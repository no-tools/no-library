use std::fmt::{Display, Formatter};

#[repr(i32)]
pub enum Error {
    InvalidUsername = 3,
    InvalidUid = 4,
    InvalidDirs(String) = 5,
    InvalidPid = 6,
}

impl Display for Error {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            Error::InvalidUsername => write!(f, "Invalid username"),
            Error::InvalidUid => write!(f, "Invalid uid"),
            Error::InvalidDirs(dir) => write!(f, "Invalid {} directory", dir),
            Error::InvalidPid => write!(f, "Invalid pid"),
        }
    }
}

impl Error {
    pub fn code(&self) -> i32 {
        unsafe { *(self as *const Self as *const i32) }
    }
}
