pub struct System {
    pub host: String,
    pub user: User,
    pub kind: Kind,
}

pub enum Kind {
    Linux(Linux),
    Mac(Mac),
    FreeBsd(FreeBsd)
}

pub struct Linux {
    pub distro: String,
    pub kernel: String,
    pub shell: String,
}

pub struct FreeBsd {
    pub release: String,
    pub kernel: String,
    pub shell: String,
}

pub struct Mac {
    pub version: String,
    pub kernel: String,
    pub shell: String,
}

impl System {
    pub fn new() -> Self {
        let host = todo!();
        let user = User::new();
        let kind = Kind::new();

        Self {
            hostname,
            user,
            kind,
        }
    }
}