use colored::Colorize;

pub use tabled::Tabled;

/// Default warning symbol
const WARN: &str = "⚠️";

/// Default crash symbol
const BURN: &str = "🔥";

/// Macro for printing a warning message, supports printf style formatting
/// # Example
/// ```
/// use no_library::warn;
///
/// warn!("This is a {}", "warning");
/// ```
///
/// # Output
/// ```notrust
/// ⚠️ This is a warning
/// ```
#[macro_export]
macro_rules! warn {
    ($($arg:tt)*) => {
        $crate::utils::warn_fn(&format!($($arg)*));
    };
}

/// Macro for printing a crash message, supports printf style formatting
/// # Example
/// ```
/// use no_library::crash;
///
/// crash!("This is a {}", "crash");
/// ```
///
/// # Output
/// ```notrust
/// 🔥 This is a crash
/// ```
#[macro_export]
macro_rules! crash {
    ($($arg:tt)*) => {
        $crate::utils::crash_fn(&format!($($arg)*));
    };
}

/// Macro for converting an arbitrary expression into a [`tabled::Table`]
/// # Example
/// ```
/// use no_library::{table, Tabled};
///
/// #[derive(Tabled)]
/// struct Person {
///    name: String,
///    age: u8,
/// }
///
/// let people = vec![
///     Person { name: "John".to_string(), age: 20 },
///     Person { name: "Jane".to_string(), age: 21 },
/// ];
///
/// let table = table!(people);
/// println!("{}", table);
/// ```
///
/// # Output
/// ```notrust
/// ┌────────┬─────┐
/// │ name   │ age │
/// ├────────┼─────┤
/// │ John   │ 20  │
/// │ Jane   │ 21  │
/// └────────┴─────┘
/// ```
#[macro_export]
macro_rules! table {
    ($struct:expr) => {
        tabled::Table::new($struct).with(tabled::Style::rounded())
    };
}

pub fn warn_fn<S: ToString>(msg: S) {
    println!("{} {}", WARN, msg.to_string().yellow());
    let _ = table!(vec![1, 2, 3]);
}

pub fn crash_fn<S: ToString>(msg: S) {
    println!("{} {}", BURN, msg.to_string().red());
}

/// Function for printing an info message.
/// This doesn't have a macro on purpose, so that each program can set its own
/// info symbol
///
/// # Example
/// ```
/// const INFO: &str = "👉";
///
/// #[macro_export]
/// macro_rules! info {
///     ($symbol:tt, $($arg:tt)*) => {
///         $crate::utils::info($symbol, &format!($($arg)*));
///     };
/// }
/// ```
pub fn info_fn<S: ToString>(symbol: &str, msg: S) {
    println!("{} {}", symbol, msg.to_string().bold());
}
